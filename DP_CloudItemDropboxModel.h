//
//  CloudItemDropboxModel.h
//  Teleprompter
//
//  Created by mrVitalya on 29.02.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudItemModel.h"
#import <DropboxSDK/DropboxSDK.h>

@interface DP_CloudItemDropboxModel : DP_CloudItemModel <DP_CloudProtocol>

@end
