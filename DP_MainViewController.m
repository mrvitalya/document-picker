//
//  DP_MainViewController.m
//  Teleprompter
//
//  Created by mrVitalya on 01.03.16.
//  Copyright © 2016 MIF. All rights reserved.
//

#import "DP_MainViewController.h"
#import "DP_MainTableViewCell.h"
#import "DP_CloudManager.h"
#import "DP_CloudProtocol.h"
#import "DP_FilePresentViewController.h"
#import "MBProgressHUD.h"

@interface DP_MainViewController ()<UITableViewDelegate, UITableViewDataSource, FilePresentDelegate>

@property (weak, nonatomic) IBOutlet UITableView *cloudTableView;
@property (strong, nonatomic) id <DP_CloudProtocol> model;
@property (nonatomic,strong) MBProgressHUD *HUD;

@end

@implementation DP_MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (AppDel.loggedin_User_Modal != nil) {
        self.cloudArray = [NSArray arrayWithObjects: @"Google Drive", @"OneDrive", @"Box", @"Dropbox", @"Local Files", nil];
    }
    else{
        self.cloudArray = [NSArray arrayWithObjects: @"Google Drive", @"OneDrive", @"Box", @"Dropbox", nil];
    }
    
    [self.cloudTableView registerNib:[UINib nibWithNibName:@"DP_MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"DP_MainTableViewCell"];
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(filePickerDidCancel)];
    self.navigationItem.leftBarButtonItem = anotherButton;
    
    [DP_CloudManager removeAllTemporaryFiles];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DP_MainTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell.cloudNameLabel.text isEqualToString:@"Google Drive"]) {
        self.model = [DP_CloudManager getModelWithName:googleDriveModelType];
     } else if ([cell.cloudNameLabel.text isEqualToString:@"Dropbox"]) {
        self.model = [DP_CloudManager getModelWithName:dropboxModelType];
    } else if ([cell.cloudNameLabel.text isEqualToString:@"Box"]) {
        self.model = [DP_CloudManager getModelWithName:boxModelType];
    } else if ([cell.cloudNameLabel.text isEqualToString:@"OneDrive"]) {
        self.model = [DP_CloudManager getModelWithName:oneDriveModelType];
    } else if ([cell.cloudNameLabel.text isEqualToString:@"Local Files"]) {
        self.model = [DP_CloudManager getModelWithName:localFilesModelType];
    }
    [self.model setRootNavigation:self.navigationController];
    [self.model setupCloud];
    self.model.docType = self.docType;
    self.model.isBulkImport = self.isBulkImport;
    
    if (self.model.cloudType != localFilesModelType) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    [self.model authenticateUserWithCompletion:^(NSError *error, BOOL completed) {
        if (completed) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.needUpload) {
                    [self.model uploadFileWithUrl:self.fileUrl completion:^(BOOL uploadComplete, NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (uploadComplete) {
                                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                [self documentPickerDidFinishUploadFile];
                            } else {
                                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                            }
                        });
                    }];
                } else {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    DP_FilePresentViewController *filePresentViewController = [[DP_FilePresentViewController alloc] init];
                    filePresentViewController.fpDelegate = self;
                    filePresentViewController.model = self.model;
                    [self.navigationController pushViewController:filePresentViewController animated:YES];
                    NSLog(@"All good");
                }
            });
        } else {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            NSLog(@"%@", error);
        }
    }];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cloudArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DP_MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DP_MainTableViewCell"];
    
    cell.imgWidth.constant = 0;
    cell.lblLeading.constant = 7;
    
    cell.cloudNameLabel.text = [self.cloudArray objectAtIndex:indexPath.row];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)filePickerDidCancel
{
    if (self.needUpload) {
        [self.documentUploadDelegate DP_FilePickerViewControllerDidCancelUpload:self];
    } else {
        [self.documentPickerDelegate DP_FilePickerViewControllerDidCancel:self];
    }
}

- (void)DP_FilePickerViewControlleDidFinishPickingFile:(NSString *)fileUrl withFileName:(NSString *)fileName
{
    [self.documentPickerDelegate DP_FilePickerViewController:self didFinishPickingFile:fileUrl withFileName:fileName];
}

- (void)DP_FilePickerViewControlleDidFinishPickingFiles:(NSArray *)arryFiles
{
    [self.documentPickerDelegate DP_FilePickerViewController:self didFinishPickingFiles:arryFiles];
}

- (void)documentPickerDidFinishUploadFile
{
    [self.documentUploadDelegate DP_FilePickerViewControllerDidFinishUpload:self];
}

- (void)DP_FilePickerViewControlleDidFinishPickingWithLocalFile:(Document *)localDoc
{
    [self.documentPickerDelegate DP_FilePickerViewController:self didFinishPickingWithLocalFile:localDoc];
}
@end
