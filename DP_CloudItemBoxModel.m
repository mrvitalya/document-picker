//
//  CloudItemBoxModel.m
//  Teleprompter
//
//  Created by mrVitalya on 29.02.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudItemBoxModel.h"
#import <BoxContentSDK/BOXContentSDK.h>
#import "DP_CloudManager.h"

@interface DP_CloudItemBoxModel()

@property (strong, nonatomic) BOXContentClient *client;

@end

@implementation DP_CloudItemBoxModel

@synthesize mainItemsArray, currentFolderLevel, rootNavigation, cloudType, docType, isBulkImport;

- (void)setupCloud
{
    self.cloudType = boxModelType;

    self.mainItemsArray = [NSMutableArray new];
    //[BOXContentClient setClientID:@"sb0q0nrw3gey2q9xqy67lqxfw011eua1" clientSecret:@"1WT9FFK9efufs0G6O6WyAgPOufvvdVOT"];
    [BOXContentClient setClientID:@"bxl8fdusu19udhvz04ovci96yyxb67y4" clientSecret:@"5NbP31Zwku2o242FglCNNrnRERJqbmXY"];

    self.client = [BOXContentClient defaultClient];

}

- (void)authenticateUserWithCompletion:(void (^)(NSError *, BOOL))completion
{
    [self.client authenticateWithCompletionBlock:^(BOXUser *user, NSError *error) {
        if (error) {
            completion(error, NO);
        } else {
            NSLog(@"All good");
            completion(nil, YES);
        }
    }];
}

- (void)logout
{
    [self.client logOut];
}

- (void)getFilesWithCompletion:(void (^)(NSArray *, NSError *))completion
{
    BOXFolderItemsRequest *itemsRequest = [self.client folderItemsRequestWithID:BOXAPIFolderIDRoot];
    itemsRequest.requestAllItemFields = YES;
    [itemsRequest performRequestWithCompletion:^(NSArray *items, NSError *error) {
        if (error == nil) {
            NSMutableArray *array = [NSMutableArray new];
            if (items.count > 0) {
                for (BOXItem *item in items) {
                    DP_CloudItemModel *model = [DP_CloudItemModel new];
                    model.itemId = item.modelID;
                    model.itemName = item.name;
                    model.isFolder = item.isFolder;
                    model.downloadUrl = item.modelID;
                    model.currentItemId = BOXAPIFolderIDRoot;
                    [array addObject:model];
                }
                self.currentFolderLevel = 0;
                [mainItemsArray addObject:array];
                completion(mainItemsArray, nil);
            } else {
                completion(mainItemsArray, nil);
            }
        } else {
            NSLog(@"ERROR %@", error);
            completion(nil, error);
        }
    }];
}

- (void)getFilesFromFolderWithFolderId:(NSString *)folderId isSubFolder:(BOOL)isSubFolder needRefresh:(BOOL)needRefresh completion:(void (^)(NSArray *, NSError *))completion
{
    BOXFolderItemsRequest *itemsRequest = [self.client folderItemsRequestWithID:folderId];
    itemsRequest.requestAllItemFields = YES;
    [itemsRequest performRequestWithCompletion:^(NSArray *items, NSError *error) {
        if (error == nil) {
            NSMutableArray *array = [NSMutableArray new];
            if (items.count > 0) {
                for (BOXItem *item in items) {
                    DP_CloudItemModel *model = [DP_CloudItemModel new];
                    model.itemId = item.modelID;
                    model.itemName = item.name;
                    model.isFolder = item.isFolder;
                    model.downloadUrl = item.modelID;
                    model.currentItemId = folderId;
                    [array addObject:model];
                }
                if (needRefresh) {
                    [mainItemsArray replaceObjectAtIndex:self.currentFolderLevel withObject:array];
                } else {
                    self.currentFolderLevel++;
                    [mainItemsArray addObject:array];
                }
                completion(mainItemsArray, nil);
            } else {
                self.currentFolderLevel++;
                [mainItemsArray addObject:array];
                completion(mainItemsArray, nil);
            }
        } else {
            NSLog(@"ERROR %@", error);
            completion(nil, error);
        }
    }];
}

- (void)downloadCloudItem:(DP_CloudItemModel *)cloudItem withCompletion:(void (^)(NSString *fileUrl, NSString *fileName, NSError *error))completion;
{
    if ([DP_CloudManager pathExtensionIsSupported:cloudItem.itemName]) {
        NSString *localFilePath = [DP_CloudManager returnTemporaryDirectoryPath];
        localFilePath = [localFilePath stringByAppendingPathExtension:[cloudItem.itemName pathExtension]];

        BOXFileDownloadRequest *boxRequest = [self.client fileDownloadRequestWithID:cloudItem.downloadUrl toLocalFilePath:localFilePath];
        [boxRequest performRequestWithProgress:^(long long totalBytesTransferred, long long totalBytesExpectedToTransfer) {
            // Update a progress bar, etc.
            
            if (totalBytesTransferred >= totalBytesExpectedToTransfer) {
                completion(localFilePath, [cloudItem.itemName stringByDeletingPathExtension], nil);
            }            
        } completion:^(NSError *error) {
            if (error) {
                NSLog(@"%@", error);
                completion(nil, nil, error);
            }
        }];
    }
    else {
        completion(nil, nil, nil);
    }
    
}

- (void)uploadFileWithUrl:(NSURL *)url completion:(void (^)(BOOL, NSError *))completion
{
    NSString *fileName = [[[url absoluteString] componentsSeparatedByString:@"/"] lastObject];
    fileName = [fileName stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
   
    NSString *localDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *localPath = [localDir stringByAppendingPathComponent:fileName];

    BOXFileUploadRequest *uploadRequest = [self.client fileUploadRequestToFolderWithID:BOXAPIFolderIDRoot fromLocalFilePath:localPath];
    
    // Optional: By default the name of the file on the local filesystem will be used as the name on Box. However, you can
    // set a different name for the file by configuring the request:
    NSArray *nameArray = [fileName componentsSeparatedByString:@"."];
    fileName = [NSString stringWithFormat:@"%@.%@", [NSString stringWithFormat:@"%@%u", [nameArray objectAtIndex:0],arc4random() % 1000], [nameArray objectAtIndex:1]];
    
    uploadRequest.fileName = fileName;
    
    [uploadRequest performRequestWithProgress:^(long long totalBytesTransferred, long long totalBytesExpectedToTransfer) {
        // Update a progress bar, etc.
    } completion:^(BOXFile *file, NSError *error) {
        if (error == nil) {
            completion(YES, nil);
        } else {
            completion(NO, error);
        }
    }];
}

@end
