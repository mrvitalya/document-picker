//
//  DP_MainTableViewCell.h
//  Teleprompter
//
//  Created by mrVitalya on 01.03.16.
//  Copyright © 2016 MIF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DP_MainTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cloudNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cloudLogoImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblLeading;
@end
