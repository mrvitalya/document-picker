//
//  DP_CloudManager.m
//  Teleprompter
//
//  Created by mrVitalya on 03.03.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudManager.h"
#import "DP_CloudItemOneDriveModel.h"
#import "DP_CloudItemBoxModel.h"
#import "DP_CloudItemDropboxModel.h"
#import "DP_CloudItemGoogleModel.h"
#import "DP_CloudItemLocalModel.h"

@implementation DP_CloudManager

+ (id)getModelWithName:(modelType)type
{
    if (type == googleDriveModelType) {
        DP_CloudItemGoogleModel *model = [DP_CloudItemGoogleModel new];
        return model;
    } else if (type == oneDriveModelType) {
        DP_CloudItemOneDriveModel *model = [DP_CloudItemOneDriveModel new];
        return model;
    } else if (type == boxModelType) {
        DP_CloudItemBoxModel *model = [DP_CloudItemBoxModel new];
        return model;
    } else if (type == dropboxModelType) {
        DP_CloudItemDropboxModel *model = [DP_CloudItemDropboxModel new];
        return model;
    } else if (type == localFilesModelType) {
        DP_CloudItemLocalModel *model = [DP_CloudItemLocalModel new];
        return model;
    }
    else {
        return nil;
    }
}

+ (BOOL)pathExtensionIsSupported:(NSString*)itemName
{
    NSString *fileExtension = [itemName pathExtension];
    if([fileExtension caseInsensitiveCompare:kIMPORTDOCFILEEXTENSION] == NSOrderedSame || [fileExtension caseInsensitiveCompare:kIMPORTDOCXFILEEXTENSION] == NSOrderedSame || [fileExtension caseInsensitiveCompare:kIMPORTTXTFILEEXTENSION] == NSOrderedSame) {
        // strings are equal except for possibly case
        return YES;
    }
    else {
        return NO;
    }
}

+ (NSString*)returnTemporaryDirectoryPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *directoryPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"PSImportFiles"];
    NSError *error;
    if (![fileManager fileExistsAtPath:directoryPath])
        [fileManager createDirectoryAtPath:directoryPath withIntermediateDirectories:NO attributes:nil error:&error];
    
    NSString *filePath = [NSString stringWithFormat:@"%ld", (NSInteger)[[NSDate date] timeIntervalSince1970]];
    NSString* newFilePath =  [directoryPath stringByAppendingPathComponent:filePath];
    
    return newFilePath;
}

+ (void)removeAllTemporaryFiles
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [NSTemporaryDirectory() stringByAppendingPathComponent:@"PSImportFiles"];
    NSError *error = nil;
    NSArray *files = [fm contentsOfDirectoryAtPath:directory error:&error];
    for (NSString *file in files) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", directory, file] error:&error];
        if (!success || error) {
            // it failed.
        }
    }
}

+ (void)removeFileFromPath:(NSString*)filePath
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fm fileExistsAtPath:filePath])
    {
        BOOL success = [fm removeItemAtPath:filePath error:&error];
        if (!success || error) {
            // it failed.
        }
    }
}
@end
