//
//  PS_DocumentPickerActivity.m
//  Teleprompter
//
//  Created by mrVitalya on 15.04.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "PS_DocumentPickerActivity.h"

@implementation PS_DocumentPickerActivity

- (NSString *)activityType
{
    return @"Document Picker";
}

- (NSString *)activityTitle
{
    return @"Document Picker";
}

- (UIImage *)activityImage
{
    return [UIImage imageNamed:@"file_picker"];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    return YES;
}

- (UIViewController *)activityViewController
{
    DP_MainViewController *docPicker = [[DP_MainViewController alloc] init];
    docPicker.documentUploadDelegate = self.documentUploadDelegate;
    docPicker.needUpload = YES;
    docPicker.fileUrl = self.fileUrl;
    
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:docPicker];
    
    return navigation;
}

@end
