//
//  DocumentPickerProtocol.h
//  Teleprompter
//
//  Created by mrVitalya on 29.02.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//


@class DP_FilePresentViewController;
@class DP_MainViewController;
@class DP_CloudItemModel;
@class Document;

#import "DP_CloudManager.h"

@protocol DocumentPickerDelegate <NSObject>

- (void)DP_FilePickerViewControllerDidCancel:(DP_MainViewController *)filePicker;
- (void)DP_FilePickerViewController:(DP_MainViewController *)filePicker didFinishPickingFile:(NSString *)fileUrl withFileName:(NSString*)fileName;
- (void)DP_FilePickerViewController:(DP_MainViewController *)filePicker didFinishPickingFiles:(NSArray *)arryFiles;

- (void)DP_FilePickerViewController:(DP_MainViewController *)filePicker didFinishPickingWithLocalFile:(Document *)localDoc;

@end

@protocol DocumentUploadDelegate <NSObject>

- (void)DP_FilePickerViewControllerDidCancelUpload:(DP_MainViewController *)filePicker;
- (void)DP_FilePickerViewControllerDidFinishUpload:(DP_MainViewController *)filePicker;

@end

@protocol FilePresentDelegate <NSObject>

- (void)DP_FilePickerViewControlleDidFinishPickingFile:(NSString *)fileUrl withFileName:(NSString*)fileName;
- (void)DP_FilePickerViewControlleDidFinishPickingFiles:(NSArray *)arryFiles;

- (void)DP_FilePickerViewControlleDidFinishPickingWithLocalFile:(Document *)localDoc;
@end

@protocol DP_CloudProtocol <NSObject>

@property (strong, nonatomic) NSMutableArray *mainItemsArray;
@property (assign, nonatomic) NSInteger currentFolderLevel;
@property (strong, nonatomic) UINavigationController *rootNavigation;
@property (readwrite, nonatomic) modelType cloudType;
@property (readwrite, nonatomic) PS_DocumentType docType;
@property (assign, nonatomic) BOOL isBulkImport;

@required

- (void)setupCloud;
- (void)authenticateUserWithCompletion:(void (^)(NSError *error, BOOL completed))completion;
- (void)downloadCloudItem:(DP_CloudItemModel *)cloudItem withCompletion:(void (^)(NSString *fileUrl, NSString *fileName, NSError *error))completion;
- (void)logout;

@optional

- (void)getFilesWithCompletion:(void (^)(NSArray *items, NSError *error))completion;
- (void)getFilesFromFolderWithFolderId:(NSString *)folderId isSubFolder:(BOOL)isSubFolder needRefresh:(BOOL)needRefresh completion:(void (^)(NSArray *items, NSError *error))completion;
- (void)uploadFileWithUrl:(NSURL *)url completion:(void (^)(BOOL uploadComplete, NSError *error))completion;

@end
