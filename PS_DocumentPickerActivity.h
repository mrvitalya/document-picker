//
//  PS_DocumentPickerActivity.h
//  Teleprompter
//
//  Created by mrVitalya on 15.04.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DocumentPicker.h"

@interface PS_DocumentPickerActivity : UIActivity

@property (retain, nonatomic) id<DocumentUploadDelegate> documentUploadDelegate;
@property (strong, nonatomic) NSURL *fileUrl;

@end
