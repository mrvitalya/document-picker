//
//  DP_FilePresentViewCell.h
//  Teleprompter
//
//  Created by mrVitalya on 03.03.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DP_FilePresentViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *itemImage;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkMarkButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *checkMarkWidth;

@end
