//
//  CloudItemModel.h
//  Teleprompter
//
//  Created by mrVitalya on 29.02.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DP_CloudProtocol.h"
#import "Document.h"

@interface DP_CloudItemModel : NSObject

@property (strong, nonatomic) NSString *itemId;
@property (strong, nonatomic) NSString *itemName;
@property (assign, nonatomic) BOOL isFolder;
@property (strong, nonatomic) NSString *downloadUrl;
@property (strong, nonatomic) NSString *currentItemId;
@property (strong, nonatomic) NSString *mimeType;

/*----- local path for bulk import -----*/
@property (strong, nonatomic) NSString *localDownloadedFilePath;

/*----- for local document -----*/
@property (strong, nonatomic) Document *localDoc;

@end
