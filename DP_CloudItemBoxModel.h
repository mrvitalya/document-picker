//
//  CloudItemBoxModel.h
//  Teleprompter
//
//  Created by mrVitalya on 29.02.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudItemModel.h"

@interface DP_CloudItemBoxModel : DP_CloudItemModel <DP_CloudProtocol>

@end
