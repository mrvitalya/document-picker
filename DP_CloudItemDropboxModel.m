//
//  CloudItemDropboxModel.m
//  Teleprompter
//
//  Created by mrVitalya on 29.02.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudItemDropboxModel.h"
#import "DP_CloudManager.h"

typedef void (^authenticateCallback)(NSError *, BOOL);
typedef void (^getFilesCallback)(NSArray *, NSError *);
typedef void (^downloadFileCallback)(NSString *, NSString *, NSError *);
typedef void (^uploadFileCallback)(BOOL, NSError *);

@interface DP_CloudItemDropboxModel()<DBRestClientDelegate>

@property (strong, nonatomic) authenticateCallback authenticateCallback;
@property (strong, nonatomic) getFilesCallback getFilesCallback;
@property (strong, nonatomic) downloadFileCallback downloadFileCallback;
@property (strong, nonatomic) uploadFileCallback uploadFileCallback;
@property (strong, nonatomic) NSString *currentFolderId;
@property (strong , nonatomic ) DBRestClient *restClient;
@property (assign, nonatomic) BOOL needRefresh;
@property (strong, nonatomic) NSString *cloudItemName;
@end

@implementation DP_CloudItemDropboxModel

@synthesize mainItemsArray, currentFolderLevel, rootNavigation, cloudType, docType, isBulkImport;

- (void)setupCloud
{
    self.cloudType = dropboxModelType;

    self.mainItemsArray = [NSMutableArray new];
    //DBSession *dbSession = [[DBSession alloc] initWithAppKey:@"p88scthliqxm789" appSecret:@"nzy1gggwkpt8fjb" root:kDBRootDropbox]; // either kDBRootAppFolder or kDBRootDropbox
    //DBSession *dbSession = [[DBSession alloc] initWithAppKey:@"fhuz53elpse5yuj" appSecret:@"bt0gqjl0qjdgo1k" root:kDBRootDropbox]; // either kDBRootAppFolder or kDBRootDropbox
    DBSession *dbSession = [[DBSession alloc] initWithAppKey:@"usnmahqnc3v8i8a" appSecret:@"ds8wxhy491yyra3" root:kDBRootDropbox]; // either kDBRootAppFolder or kDBRootDropbox
    
    [DBSession setSharedSession:dbSession];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authenticationComplete:) name:@"dropboxCompletion" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeSubFolderName) name:@"backButtonClick" object:nil];

}

- (void)authenticateUserWithCompletion:(void (^)(NSError *, BOOL))completion
{
    if (![[DBSession sharedSession] isLinked]){
        self.authenticateCallback = completion;
        [[DBSession sharedSession] linkFromController:self.rootNavigation];
    } else{
        completion(nil, YES);
        NSLog(@"App linked");
    }
}

- (void)logout
{
    [[DBSession sharedSession] unlinkAll];
}

- (void)getFilesWithCompletion:(void (^)(NSArray *, NSError *))completion
{
    self.getFilesCallback = completion;
    self.currentFolderId = @"/";
    [self.restClient loadMetadata:@"/"];
}

- (void)getFilesFromFolderWithFolderId:(NSString *)folderId isSubFolder:(BOOL)isSubFolder needRefresh:(BOOL)needRefresh completion:(void (^)(NSArray *, NSError *))completion
{
    self.needRefresh = needRefresh;
    self.getFilesCallback = completion;
    if (needRefresh) {
        self.currentFolderId = folderId;
    } else {
        NSString *requesFileFromDirectory = [NSString stringWithFormat:@"%@", folderId];
        if (isSubFolder) {
            self.currentFolderId = [NSString stringWithFormat:@"%@/%@", self.currentFolderId, folderId];
        } else {
            self.currentFolderId = requesFileFromDirectory;
        }
    }
    [self.restClient loadMetadata:self.currentFolderId];
}

- (void)downloadCloudItem:(DP_CloudItemModel *)cloudItem withCompletion:(void (^)(NSString *fileUrl, NSString *fileName, NSError *error))completion;
{
    self.downloadFileCallback = completion;
    
    if ([DP_CloudManager pathExtensionIsSupported:cloudItem.itemName]) {
        self.cloudItemName = cloudItem.itemName;
        NSString *tmpFileName = [DP_CloudManager returnTemporaryDirectoryPath];
        tmpFileName = [tmpFileName stringByAppendingPathExtension:[cloudItem.itemName pathExtension]];
        [self.restClient loadFile:cloudItem.downloadUrl intoPath:tmpFileName];
    }
    else{
        if (self.downloadFileCallback) {
            self.downloadFileCallback(nil, nil, nil);
        }
    }
}

- (void)uploadFileWithUrl:(NSURL *)url completion:(void (^)(BOOL, NSError *))completion
{
    NSString *fileName = [[[url absoluteString] componentsSeparatedByString:@"/"] lastObject];
    
    fileName = [fileName stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    

    NSString *localDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *localPath = [localDir stringByAppendingPathComponent:fileName];

    self.uploadFileCallback = completion;

    [self.restClient uploadFile:fileName toPath:@"/" withParentRev:nil fromPath:localPath];
}

- (void)authenticationComplete:(NSNotification *)notification
{
    if ([[notification.userInfo objectForKey:@"complete"] boolValue]) {
        if (self.authenticateCallback) {
            self.authenticateCallback(nil, YES);
        }
    } else {
        if (self.authenticateCallback) {
            self.authenticateCallback(nil, NO);
        }
    }
    NSLog(@"%@", notification);
}

- (DBRestClient *)restClient {
    if (!_restClient) {
        _restClient =
        [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        _restClient.delegate = self;
    }
    return _restClient;
}

- (void)restClient:(DBRestClient *)client loadedFile:(NSString *)localPath
       contentType:(NSString *)contentType metadata:(DBMetadata *)metadata {
    if (self.downloadFileCallback) {
        self.downloadFileCallback(localPath, self.cloudItemName, nil);
    }
    NSLog(@"File loaded into path: %@", localPath);
}

- (void)restClient:(DBRestClient *)client loadFileFailedWithError:(NSError *)error {
    NSLog(@"There was an error loading the file: %@", error);
    
    if (self.downloadFileCallback) {
        self.downloadFileCallback(nil, nil, error);
    }
}

- (void)restClient:(DBRestClient *)client uploadedFile:(NSString *)destPath
              from:(NSString *)srcPath metadata:(DBMetadata *)metadata {
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
    if (self.uploadFileCallback) {
        self.uploadFileCallback(YES, nil);
    }
}

- (void)restClient:(DBRestClient *)client uploadFileFailedWithError:(NSError *)error {
    NSLog(@"File upload failed with error: %@", error);
    if (self.uploadFileCallback) {
        self.uploadFileCallback(NO, error);
    }
}

- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata {
    if (metadata.isDirectory) {
        NSLog(@"Folder '%@' contains:", metadata.path);
        NSMutableArray *array = [NSMutableArray new];
        if (metadata.contents.count > 0) {
            for (DBMetadata *file in metadata.contents) {
                DP_CloudItemModel *model = [DP_CloudItemModel new];
                model.itemId = file.filename;
                model.itemName = file.filename;
                model.isFolder = file.isDirectory;
                model.downloadUrl = file.path;
                model.currentItemId = self.currentFolderId;
                [array addObject:model];
            }
            if (self.needRefresh) {
                [self.mainItemsArray replaceObjectAtIndex:self.currentFolderLevel withObject:array];
            } else {
                [mainItemsArray addObject:array];
                if ([self.currentFolderId isEqualToString:@"/"]) {
                    self.currentFolderLevel = 0;
                } else {
                    self.currentFolderLevel++;
                }
            }
            if (self.getFilesCallback) {
                self.getFilesCallback(mainItemsArray, nil);
            }
        } else {
            [self removeSubFolderName];
            self.currentFolderLevel++;
            [mainItemsArray addObject:array];
            if (self.getFilesCallback) {
                self.getFilesCallback(mainItemsArray, nil);
            }
        }
    } else {
        if (self.getFilesCallback) {
            self.getFilesCallback(nil, nil);
        }
    }
}

- (void)restClient:(DBRestClient *)client loadMetadataFailedWithError:(NSError *)error {
    NSLog(@"Error loading metadata: %@", error);
    if (self.getFilesCallback) {
        self.getFilesCallback(nil, error);
    }
}

- (void)removeSubFolderName
{
    NSArray *listItems = [self.currentFolderId componentsSeparatedByString:@"/"];
    NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:listItems];
    [tmpArray removeLastObject];
    self.currentFolderId = [tmpArray componentsJoinedByString:@"/"];
}

@end
