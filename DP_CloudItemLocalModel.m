//
//  DP_CloudItemLocalModel.m
//  Teleprompter
//
//  Created by Chirag Patel on 21/10/16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudItemLocalModel.h"
#import "Document.h"
#import "PS_SyncDocument.h"

@implementation DP_CloudItemLocalModel

@synthesize mainItemsArray, currentFolderLevel, rootNavigation, cloudType, docType, isBulkImport;

- (void)setupCloud
{
    self.cloudType = localFilesModelType;
    self.mainItemsArray = [NSMutableArray new];
}

- (void)authenticateUserWithCompletion:(void (^)(NSError *, BOOL))completion
{
    if (completion) {
        completion(nil, YES);
    }
}

- (void)logout
{
}

- (void)getFilesWithCompletion:(void (^)(NSArray *, NSError *))completion
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Document"];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(%K=%@ || %K==nil) && (%K=%@ || %K==nil) && %K=%@", kISDELETED, [NSNumber numberWithBool:NO], kISDELETED, kUserID, [NSNumber numberWithInteger:0], kUserID, kTYPE, [NSNumber numberWithBool:self.docType]];

    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    
    NSArray *items = [AppDel.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *array = [NSMutableArray new];
    if (items.count > 0) {
        for (Document *item in items) {
            
            if (![kDocSyncSharedInst.defaultDocuments containsObject:item.name]) {
                DP_CloudItemModel *model = [DP_CloudItemModel new];
                model.itemId = [item.objectid stringValue];
                model.itemName = item.name;
                model.isFolder = NO;
                model.downloadUrl = item.fileurl;
                model.currentItemId = @"root";
                model.localDoc = item;
                [array addObject:model];
            }
        }
        self.currentFolderLevel = 0;
        [mainItemsArray addObject:array];
        completion(mainItemsArray, nil);
    } else {
        completion(mainItemsArray, nil);
    }
}

- (void)downloadCloudItem:(DP_CloudItemModel *)cloudItem withCompletion:(void (^)(NSString *fileUrl, NSString *fileName, NSError *error))completion;
{
    completion(cloudItem.itemId, cloudItem.localDoc.name, nil);
}


@end
