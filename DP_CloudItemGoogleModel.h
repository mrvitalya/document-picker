//
//  CloudItemGoogleModel.h
//  Teleprompter
//
//  Created by mrVitalya on 29.02.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudItemModel.h"
#import "GTLDrive.h"
#import "DP_MainViewController.h"

@interface DP_CloudItemGoogleModel : DP_CloudItemModel <DP_CloudProtocol>

@end
