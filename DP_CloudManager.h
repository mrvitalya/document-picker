//
//  DP_CloudManager.h
//  Teleprompter
//
//  Created by mrVitalya on 03.03.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    googleDriveModelType,
    boxModelType,
    dropboxModelType,
    oneDriveModelType,
    localFilesModelType
} modelType;


@interface DP_CloudManager : NSObject

+ (id)getModelWithName:(modelType)type;

+ (BOOL)pathExtensionIsSupported:(NSString*)itemName;

+ (NSString*)returnTemporaryDirectoryPath;

+ (void)removeAllTemporaryFiles;

+ (void)removeFileFromPath:(NSString*)filePath;
@end
