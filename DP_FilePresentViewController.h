//
//  DP_FilePresentViewController.h
//  Teleprompter
//
//  Created by mrVitalya on 03.03.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DP_CloudProtocol.h"

@interface DP_FilePresentViewController : UIViewController

@property (strong, nonatomic) id <DP_CloudProtocol>model;
@property (retain, nonatomic) id <FilePresentDelegate>fpDelegate;

@end
