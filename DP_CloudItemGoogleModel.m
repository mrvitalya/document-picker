//
//  CloudItemGoogleModel.m
//  Teleprompter
//
//  Created by mrVitalya on 29.02.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudItemGoogleModel.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "DP_CloudManager.h"

//static NSString *const kKeychainItemName = @"MIF.GDriveTest";
//static NSString *const kClientID = @"26115693799-2sltgmh1t3t5ilna31voc8ievcctrjbb.apps.googleusercontent.com";
static NSString *const kKeychainItemName = @"FP.promptsmart";
static NSString *const kClientID = @"182169297304-g8rsnhrvlclao8b5ulm05a5lvlocifgk.apps.googleusercontent.com";

/*----- google drive possible document mimeType supported by our app -----*/
static NSArray *googleDriveDocTypes = nil;
static NSString *const kGoogleDocsType = @"application/vnd.google-apps.document";

typedef void (^authenticateCallback)(NSError *, BOOL);

@interface DP_CloudItemGoogleModel()

@property (strong, nonatomic) authenticateCallback callback;
@property (nonatomic, strong) GTLServiceDrive *service;

@end

@implementation DP_CloudItemGoogleModel

@synthesize mainItemsArray, currentFolderLevel, rootNavigation, cloudType, docType, isBulkImport;

- (void)setupCloud
{
    self.cloudType = googleDriveModelType;

    self.mainItemsArray = [NSMutableArray new];
    self.service = [[GTLServiceDrive alloc] init];
    self.service.authorizer =
    [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                          clientID:kClientID
                                                      clientSecret:nil];
    
    /*---- should support only these mimetype in our app -----*/
    if (googleDriveDocTypes == nil) {
      googleDriveDocTypes = @[@"application/vnd.google-apps.document", @"text/plain", @"application/msword", @"application/vnd.openxmlformats-officedocument.wordprocessingml.document"];
    }

}

- (void)authenticateUserWithCompletion:(void (^)(NSError *, BOOL))completion
{
    if (!self.service.authorizer.canAuthorize) {
        self.callback = completion;
        
        NSArray *scopes = [NSArray arrayWithObjects:kGTLAuthScopeDrive, nil];
        
        GTMOAuth2ViewControllerTouch *authController = [[GTMOAuth2ViewControllerTouch alloc] initWithScope:[scopes componentsJoinedByString:@" "]
                                                                    clientID:kClientID
                                                                clientSecret:nil
                                                            keychainItemName:kKeychainItemName
                                                                    delegate:self
                                                            finishedSelector:@selector(viewController:finishedWithAuth:error:)];
        [self.rootNavigation pushViewController:authController animated:YES];
    } else {
        completion(nil, YES);
        NSLog(@"Authentification complete");
    }
}

- (void)logout
{
    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];
}

- (void)getFilesWithCompletion:(void (^)(NSArray *, NSError *))completion
{
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    
    query.q = [NSString stringWithFormat:@"'%@' IN parents and trashed=false", @"root"];
    
    // queryTicket can be used to track the status of the request.
    [self.service executeQuery:query
             completionHandler:^(GTLServiceTicket *ticket,
                                 GTLDriveFileList *files, NSError *error){
                 if (error == nil) {
                     NSMutableArray *array = [NSMutableArray new];
                     if (files.files.count > 0) {
                         for (GTLDriveFile *file in files.files) {
                             DP_CloudItemModel *model = [DP_CloudItemModel new];
                             model.itemId = file.identifier;
                             model.itemName = file.name;
                             model.isFolder = [file.mimeType isEqualToString:@"application/vnd.google-apps.folder"];
                             model.downloadUrl = file.identifier;
                             model.currentItemId = @"root";
                             model.mimeType = file.mimeType;
                             [array addObject:model];
                         }
                         self.currentFolderLevel = 0;
                         [mainItemsArray addObject:array];
                         completion(mainItemsArray, nil);
                     } else {
                         completion(mainItemsArray, nil);
                     }
                 } else {
                     NSLog(@"ERROR %@", error);
                     completion(nil, error);
                 }
             }];
}

- (void)downloadCloudItem:(DP_CloudItemModel *)cloudItem withCompletion:(void (^)(NSString *fileUrl, NSString *fileName, NSError *error))completion;
{
    NSString *fileExtension = [cloudItem.itemName pathExtension];

    if ([DP_CloudManager pathExtensionIsSupported:cloudItem.itemName] || fileExtension == nil || fileExtension.length == 0) {
        
        if ([googleDriveDocTypes containsObject:cloudItem.mimeType]) {
            
            NSString *url = nil;
            
            if ([cloudItem.mimeType isEqualToString:kGoogleDocsType]) { //should get content through export
                url = [NSString stringWithFormat:@"https://www.googleapis.com/drive/v3/files/%@/export?alt=media&mimeType=text/plain", cloudItem.downloadUrl];
            }
            else{
                url = [NSString stringWithFormat:@"https://www.googleapis.com/drive/v3/files/%@?alt=media",cloudItem.downloadUrl];
            }
            
            GTMSessionFetcher *fetcher = [self.service.fetcherService fetcherWithURLString:url];
            
            [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
                
                if (error == nil) {
                    //NSLog(@"Retrieved file content");
                    // Do something with data
                    NSString *newFilePath = [DP_CloudManager returnTemporaryDirectoryPath];

                    if (fileExtension == nil || fileExtension.length == 0) {
                        newFilePath = [newFilePath stringByAppendingString:[self returnFileExtension:cloudItem.mimeType]];
                    }
                    else{
                        newFilePath = [newFilePath stringByAppendingPathExtension:fileExtension];
                    }
                    
                    [data writeToFile:newFilePath atomically:YES];
                    
                    completion(newFilePath, [cloudItem.itemName stringByDeletingPathExtension], nil);
                } else {
                    NSLog(@"An error occurred: %@", error);
                    completion(nil, nil, error);
                }
            }];
        }
        else{
            completion(nil, nil, nil);
        }
    }
    else{
        completion(nil, nil, nil);
    }
}

- (NSString*)returnFileExtension:(NSString*)mimetype
{
    NSDictionary *dicExtension = @{@"application/vnd.openxmlformats-officedocument.wordprocessingml.document" : @".docx", @"application/msword" : @".doc", @"text/plain" : @".txt", @"application/vnd.google-apps.document" : @".txt"};
    
    return [dicExtension valueForKey:mimetype];
}

- (void)getFilesFromFolderWithFolderId:(NSString *)folderId isSubFolder:(BOOL)isSubFolder needRefresh:(BOOL)needRefresh completion:(void (^)(NSArray *, NSError *))completion
{
    
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    query.q = [NSString stringWithFormat:@"'%@' IN parents and trashed=false", folderId];
    
    // queryTicket can be used to track the status of the request.
    [self.service executeQuery:query
             completionHandler:^(GTLServiceTicket *ticket,
                                 GTLDriveFileList *files, NSError *error){
                 if (error == nil) {
                     NSMutableArray *array = [NSMutableArray new];
                     if (files.files.count > 0) {
                         for (GTLDriveFile *file in files.files) {

                             DP_CloudItemModel *model = [DP_CloudItemModel new];
                             model.itemId = file.identifier;
                             model.itemName = file.name;
                             model.isFolder = [file.mimeType isEqualToString:@"application/vnd.google-apps.folder"];
                             model.downloadUrl = file.identifier;
                             model.currentItemId = folderId;
                             model.mimeType = file.mimeType;
                             
                             [array addObject:model];
                         }
                         if (needRefresh) {
                             [mainItemsArray replaceObjectAtIndex:self.currentFolderLevel withObject:array];
                         } else {
                             [mainItemsArray addObject:array];
                         }
                         if ([folderId isEqualToString:@"root"]) {
                             self.currentFolderLevel = 0;
                         } else {
                             self.currentFolderLevel++;
                         }

                        completion(mainItemsArray, nil);
                     } else {
                         if (needRefresh) {
                             [mainItemsArray replaceObjectAtIndex:self.currentFolderLevel withObject:array];
                         } else {
                             [mainItemsArray addObject:array];
                         }
                         if ([folderId isEqualToString:@"root"]) {
                             self.currentFolderLevel = 0;
                         } else {
                             self.currentFolderLevel++;
                         }
                         completion(mainItemsArray, nil);
                     }
                 } else {
                     NSLog(@"ERROR %@", error);
                     completion(nil, error);
                 }
             }];
}

- (void)uploadFileWithUrl:(NSURL *)url completion:(void (^)(BOOL, NSError *))completion
{
    
    NSString *fileName = [[[url absoluteString] componentsSeparatedByString:@"/"] lastObject];
    
    fileName = [fileName stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    
    GTLUploadParameters *uploadParameters =
    [GTLUploadParameters uploadParametersWithFileURL:url
                                            MIMEType:@""];
    GTLDriveFile *newFile = [GTLDriveFile object];
    newFile.name = fileName;
    
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesCreateWithObject:newFile
                                                       uploadParameters:uploadParameters];
    [self.service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLDriveFile *uploadedFile, NSError *error) {
        if (error == nil) {
            completion(YES, nil);
        } else {
            completion(NO, error);
        }
    }];
}


- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)authResult
                 error:(NSError *)error {
    if (error != nil) {
        self.service.authorizer = nil;
        if (self.callback) {
            self.callback(error, NO);
        }
    } else {
        self.service.authorizer = authResult;
        if (self.callback) {
            self.callback(nil, YES);
        }
    }
}

@end
