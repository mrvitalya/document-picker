//
//  DP_MainViewController.h
//  Teleprompter
//
//  Created by mrVitalya on 01.03.16.
//  Copyright © 2016 MIF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DP_CloudProtocol.h"

@interface DP_MainViewController : UIViewController

@property (strong, nonatomic) NSArray *cloudArray;
@property (retain, nonatomic) id <DocumentPickerDelegate>documentPickerDelegate;
@property (retain, nonatomic) id <DocumentUploadDelegate>documentUploadDelegate;
@property (assign, nonatomic) BOOL needUpload;
@property (strong, nonatomic) NSURL *fileUrl;
@property (assign, nonatomic) BOOL isBulkImport;
@property (readwrite, nonatomic) PS_DocumentType docType;
@end
