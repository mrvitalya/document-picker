//
//  DP_FilePresentViewController.m
//  Teleprompter
//
//  Created by mrVitalya on 03.03.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_FilePresentViewController.h"
#import "DP_FilePresentViewCell.h"
#import "DP_CloudItemModel.h"
#import "MBProgressHUD.h"
#import "PS_GlobalMethods.h"
#import "DP_CloudManager.h"

@interface DP_FilePresentViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *itemsArray;
@property (weak, nonatomic) IBOutlet UITableView *itemsTableView;
@property (nonatomic,strong) MBProgressHUD *HUD;

@property (strong, nonatomic) DP_CloudItemModel *currentModel;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

/*----- bulk import view -----*/
@property (weak, nonatomic) IBOutlet UIView *importView;
@property (weak, nonatomic) IBOutlet UIButton *importButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itemsTableViewBottom;

@property (strong, nonatomic) NSMutableArray *selectedItemsArray;

@end

@implementation DP_FilePresentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(backButtonClick)];
    self.navigationItem.leftBarButtonItem = addButton;
    
    if (self.model.cloudType != localFilesModelType) {
        UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logoutButtonClick)];
        self.navigationItem.rightBarButtonItem = logoutButton;
        
        self.refreshControl = [UIRefreshControl new];
        [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
        [self.itemsTableView addSubview:self.refreshControl];
    }
    
    self.itemsTableViewBottom.constant = 0;

    if (self.model.isBulkImport) {
        
        /*----- allocate mutable array - bulk import  -----*/
        self.selectedItemsArray = [[NSMutableArray alloc] init];

        /*----- set shadow to view -----*/
        self.importView.layer.shadowColor = [UIColor blackColor].CGColor;
        self.importView.layer.shadowRadius = 1;
        self.importView.layer.shadowOpacity = 0.2;
        self.importView.layer.shadowOffset = CGSizeMake(0, 0);
        self.importView.layer.masksToBounds = NO;
        
        /*----- set border to button -----*/
        self.importButton.layer.cornerRadius = 10;
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.itemsTableView registerNib:[UINib nibWithNibName:@"DP_FilePresentViewCell" bundle:nil] forCellReuseIdentifier:@"DP_FilePresentViewCell"];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.model getFilesWithCompletion:^(NSArray *items, NSError *error) {
        if (error == nil) {
            if (items.count > 0) {
                if ([[items objectAtIndex:[self.model currentFolderLevel]] count] > 0) {
                    self.itemsArray = items;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self.itemsTableView reloadData];
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                        [self setUpFooterView];
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSString *strMessage = @"";
                        if (self.model.cloudType == localFilesModelType) {
                            strMessage = @"There are no local files to import";
                        }
                        else{
                            strMessage = @"This folder empty";
                        }
                        
                        UIAlertView *alert_error = [[UIAlertView alloc]initWithTitle:nil message:strMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"strOK", nil), nil];
                        [alert_error show];
                        NSLog(@"Dont found files");
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    });
                }
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert_error = [[UIAlertView alloc]initWithTitle:nil message:@"This cloud service empty" delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"strOK", nil), nil];
                    [alert_error show];
                    NSLog(@"Dont found files");
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                });
            }
        } else {
            NSLog(@"ERROR");
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
    }];
}
- (void)backButtonClick
{
    if (self.model.currentFolderLevel > 0) {
        [[self.model mainItemsArray] removeObjectAtIndex:[self.model currentFolderLevel]];
        self.model.currentFolderLevel--;
        [self.itemsTableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"backButtonClick" object:nil];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)logoutButtonClick
{
    [self.model logout];
    self.model.currentFolderLevel = 0;
    [self backButtonClick];
}

/*----- set up footerview  -----*/
- (void)setUpFooterView
{
    if (self.model.isBulkImport) {
        self.itemsTableViewBottom.constant = 80;
    }
    else {
        self.itemsTableViewBottom.constant = 0;
    }
    
    [self enableImportButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.itemsArray objectAtIndex:[self.model currentFolderLevel]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DP_FilePresentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DP_FilePresentViewCell"];
    DP_CloudItemModel *currentModel = [[self.itemsArray objectAtIndex:[self.model currentFolderLevel]] objectAtIndex:indexPath.row];
    cell.itemNameLabel.text = currentModel.itemName;
    if (currentModel.isFolder) {
        [cell.itemImage setImage:[UIImage imageNamed:@"folder_img.png"]];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        [cell.itemImage setImage:[UIImage imageNamed:@"file_img.png"]];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if (!currentModel.isFolder && self.model.isBulkImport) {
        cell.checkMarkWidth.constant = 22.0f;
        cell.checkMarkButton.tag = indexPath.row;
        [cell.checkMarkButton addTarget:self action:@selector(clickOnCheckMark:) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        cell.checkMarkWidth.constant = 0.0f;
    }
    
    [cell.checkMarkButton setSelected:NO];

    if (self.model.isBulkImport) {
        
        if (self.model.cloudType == localFilesModelType) {
            if ([self.selectedItemsArray containsObject:currentModel.localDoc]) {
                [cell.checkMarkButton setSelected:YES];
            }
        }
        else{
            NSArray *arryFileURLs = [self.selectedItemsArray valueForKey:@"fileURL"];
            if ([arryFileURLs containsObject:currentModel.localDownloadedFilePath] && currentModel.localDownloadedFilePath != nil) {
                [cell.checkMarkButton setSelected:YES];
            }
        }
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DP_CloudItemModel *currentModel = [[self.itemsArray objectAtIndex:[self.model currentFolderLevel]] objectAtIndex:indexPath.row];
    DP_FilePresentViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    if (currentModel.isFolder) {
        NSLog(@"It is folder");
        [self loadFilesFromSubfolderAndRefresh:currentModel.itemId isSubfolder:YES needRefresh:NO];
     } else {
         if (self.model.isBulkImport) {
             [self clickOnCheckMark:cell.checkMarkButton];
         }
         else {
            [self downloadCloudItem:currentModel];
         }
    }
}

- (void)clickOnCheckMark:(id)sender
{
    UIButton *checkMarkButton = (UIButton*)sender;
    DP_CloudItemModel *currentModel = [[self.itemsArray objectAtIndex:[self.model currentFolderLevel]] objectAtIndex:[checkMarkButton tag]];

    if ([checkMarkButton isSelected]) {
        
        if (self.model.cloudType == localFilesModelType) {
            
            if ([self.selectedItemsArray containsObject:currentModel.localDoc]) {
                [self.selectedItemsArray removeObject:currentModel.localDoc];
            }
        }
        else{
            NSArray *arryFileURLs = [self.selectedItemsArray valueForKey:@"fileURL"];
            if ([arryFileURLs containsObject:currentModel.localDownloadedFilePath] && currentModel.localDownloadedFilePath != nil) {
                
                NSInteger idx = [arryFileURLs indexOfObject:currentModel.localDownloadedFilePath];
                if(idx != NSNotFound) {
                    [DP_CloudManager removeFileFromPath:currentModel.localDownloadedFilePath];
                    currentModel.localDownloadedFilePath = nil;
                    [self.selectedItemsArray removeObjectAtIndex:idx];
                }
            }
        }
        
        [self enableImportButton];
        [self.itemsTableView reloadData];
    }
    else{
        [self downloadCloudItem:currentModel];
    }
}

- (void)downloadCloudItem:(DP_CloudItemModel*)currentModel
{
    //NSLog(@"It is file");
    if (self.model.cloudType != localFilesModelType) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }

    [self.model downloadCloudItem:currentModel withCompletion:^(NSString *fileUrl, NSString *fileName, NSError *error) {
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if (error != nil) {
            [PS_GlobalMethods showAlertwithTitle:nil withMessage:error.localizedDescription withViewController:self withCompletion:nil];
            [self.itemsTableView reloadData];
        }
        else if (fileUrl == nil || fileUrl.length == 0) {
            [PS_GlobalMethods showAlertwithTitle:NSLocalizedString(@"strImportErrorTitle", nil) withMessage:NSLocalizedString(@"strImportErrorMessage", nil) withViewController:self withCompletion:nil];
            [self.itemsTableView reloadData];
        }
        else
        {
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"\\/:?*\"|"];
            fileName = [[fileName componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @"_"];

            if (self.model.isBulkImport) {
                
                if (self.model.cloudType == localFilesModelType) {
                    [self.selectedItemsArray addObject:currentModel.localDoc];
                }
                else{
                    NSDictionary *dicFile = @{@"fileURL" : fileUrl, @"fileName" : fileName};
                    [self.selectedItemsArray addObject:dicFile];
                    currentModel.localDownloadedFilePath = fileUrl;
                }
                
                [self enableImportButton];
                [self.itemsTableView reloadData];
            }
            else{
                if (currentModel.localDoc != nil) {
                    [self didFinishPickingWithLocalFile:currentModel.localDoc];
                }
                else{
                    [self didFinishPickingFile:fileUrl withFileName:fileName];
                }
            }
        }
    }];
}

- (void)refresh:(id)sender
{
    if ([sender isKindOfClass:[UIRefreshControl class]]) {
        DP_CloudItemModel *currentModel = [[self.itemsArray objectAtIndex:[self.model currentFolderLevel]] objectAtIndex:0];
        [self loadFilesFromSubfolderAndRefresh:currentModel.currentItemId isSubfolder:YES needRefresh:YES];
    }
}

- (void)loadFilesFromSubfolderAndRefresh:(NSString *)folderId isSubfolder:(BOOL)isSubfolder needRefresh:(BOOL)needRefresh
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.model getFilesFromFolderWithFolderId:folderId isSubFolder:isSubfolder needRefresh:needRefresh completion:^(NSArray *items, NSError *error) {
        if (error == nil) {
            if (items.count > 0) {
                if ([[items objectAtIndex:[self.model currentFolderLevel]] count] > 0) {
                    self.itemsArray = items;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        [self.itemsTableView reloadData];
                        [self.refreshControl endRefreshing];
                });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView *alert_error = [[UIAlertView alloc]initWithTitle:nil message:@"This folder empty" delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"strOK", nil), nil];
                        [alert_error show];
                        NSLog(@"Dont found files");
                        [[self.model mainItemsArray] removeObjectAtIndex:[self.model currentFolderLevel]];
                        self.model.currentFolderLevel--;
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        [self.refreshControl endRefreshing];
                        [self.refreshControl endRefreshing];
                    });
                }
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert_error = [[UIAlertView alloc]initWithTitle:nil message:@"This cloud service empty" delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"strOK", nil), nil];
                    [alert_error show];
                    NSLog(@"Dont found files");
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [self.itemsTableView reloadData];
                    [self.refreshControl endRefreshing];
                });
            }
        } else {
            NSLog(@"ERROR");
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.refreshControl endRefreshing];
        }
    }];
}
- (void)didFinishPickingFile:(NSString *)fileUrl withFileName:(NSString *)fileName
{
    [self.fpDelegate DP_FilePickerViewControlleDidFinishPickingFile:fileUrl withFileName:fileName];
}

- (void)didFinishPickingWithLocalFile:(Document *)localDoc
{
    [self.fpDelegate DP_FilePickerViewControlleDidFinishPickingWithLocalFile:localDoc];
}

- (BOOL)itemExistAtPath:(NSString*)name
{
    NSString *documentsDirectory = NSTemporaryDirectory();
    NSString *tmpFileName = [documentsDirectory stringByAppendingPathComponent:name];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:tmpFileName];
    
    return fileExists;
}

- (void)deleteItemAtPath:(NSString*)name
{
    NSString *documentsDirectory = NSTemporaryDirectory();
    NSString *tmpFileName = [documentsDirectory stringByAppendingPathComponent:name];
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:tmpFileName error:&error];
}

- (void)enableImportButton
{
    if (self.selectedItemsArray.count > 0) {
        self.importButton.enabled = YES;
        self.importButton.alpha = 1.0f;
    }
    else{
        self.importButton.enabled = NO;
        self.importButton.alpha = 0.5f;
    }
}

#pragma mark - Click on import button ~ bulk import
- (IBAction)clickOnImport:(id)sender
{
    [self.fpDelegate DP_FilePickerViewControlleDidFinishPickingFiles:self.selectedItemsArray];
}

@end
