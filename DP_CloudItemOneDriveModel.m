//
//  CloudItemOneDriveModel.m
//  Teleprompter
//
//  Created by mrVitalya on 29.02.16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudItemOneDriveModel.h"
#import "DP_CloudManager.h"

@interface DP_CloudItemOneDriveModel()

@property (strong, nonatomic) ODClient *odClient;

@end

@implementation DP_CloudItemOneDriveModel

@synthesize mainItemsArray, currentFolderLevel, rootNavigation, cloudType, docType, isBulkImport;

- (void)setupCloud
{
    self.cloudType = oneDriveModelType;
    self.mainItemsArray = [NSMutableArray new];
    //[ODClient setMicrosoftAccountAppId:@"000000004C183D16" scopes:@[@"wl.signin", @"onedrive.readwrite", @"wl.offline_access"]];
    [ODClient setMicrosoftAccountAppId:@"205d6e2a-f62b-4e81-9d00-07de1042c423" scopes:@[@"wl.signin", @"onedrive.readwrite", @"wl.offline_access"]];
}

- (void)authenticateUserWithCompletion:(void (^)(NSError *, BOOL))completion
{
    [ODClient clientWithCompletion:^(ODClient *client, NSError *error) {
        if (!error) {
            self.odClient = client;
            
            if (completion) {
                completion(nil, YES);
            }
        } else {
            if (completion) {
                completion(error, NO);
            }
        }
    }];
}

- (void)logout
{
    [self.odClient signOutWithCompletion:nil];
}

- (void)getFilesWithCompletion:(void (^)(NSArray *, NSError *))completion
{
    [[[[[self.odClient drive] items:@"root"] children] request] getWithCompletion:^(ODCollection *response, ODChildrenCollectionRequest *nextRequest, NSError *error) {
        if (error == nil) {
            NSMutableArray *array = [NSMutableArray new];
            if (response.value.count > 0) {
                for (ODItem *file in response.value) {
                    DP_CloudItemModel *model = [DP_CloudItemModel new];
                    model.itemId = file.id;
                    model.itemName = file.name;
                    model.isFolder = file.folder;
                    model.downloadUrl = file.id;
                    model.currentItemId = @"root";
                    [array addObject:model];
                }
                self.currentFolderLevel = 0;
                [mainItemsArray addObject:array];
                completion(mainItemsArray, nil);
            } else {
                completion(mainItemsArray, nil);
            }
        } else {
            NSLog(@"ERROR %@", error);
            completion(nil, error);
        }
    }];
}

- (void)getFilesFromFolderWithFolderId:(NSString *)folderId isSubFolder:(BOOL)isSubFolder needRefresh:(BOOL)needRefresh completion:(void (^)(NSArray *, NSError *))completion
{
    [[[[[self.odClient drive] items:folderId] children] request] getWithCompletion:^(ODCollection *response, ODChildrenCollectionRequest *nextRequest, NSError *error) {
        if (error == nil) {
            NSMutableArray *array = [NSMutableArray new];
            if (response.value.count > 0) {
                for (ODItem *file in response.value) {
                    DP_CloudItemModel *model = [DP_CloudItemModel new];
                    model.itemId = file.id;
                    model.itemName = file.name;
                    model.isFolder = file.folder;
                    model.downloadUrl = file.id;
                    model.currentItemId = folderId;
                    [array addObject:model];
                }
                if (needRefresh) {
                    [mainItemsArray replaceObjectAtIndex:currentFolderLevel withObject:array];
                } else {
                    self.currentFolderLevel++;
                    [mainItemsArray addObject:array];
                }
                completion(mainItemsArray, nil);
            } else {
                self.currentFolderLevel++;
                [mainItemsArray addObject:array];
                completion(mainItemsArray, nil);
            }
        } else {
            NSLog(@"ERROR %@", error);
            completion(nil, error);
        }
    }];

}

- (void)downloadCloudItem:(DP_CloudItemModel *)cloudItem withCompletion:(void (^)(NSString *fileUrl, NSString *fileName, NSError *error))completion;
{
    if ([DP_CloudManager pathExtensionIsSupported:cloudItem.itemName]) {
        ODItemContentRequest *request = [[[self.odClient drive] items:cloudItem.downloadUrl] contentRequest];
        
        [request downloadWithCompletion:^(NSURL *filePath, NSURLResponse *urlResponse, NSError *error){
            if (error) {
                completion(nil, nil, error);
            } else {
                
                NSData *myData = [NSData dataWithContentsOfFile:(NSString *)filePath];
                
                NSString *newFilePath = [DP_CloudManager returnTemporaryDirectoryPath];
                newFilePath = [newFilePath stringByAppendingPathExtension:[cloudItem.itemName pathExtension]];
                
                [myData writeToFile:newFilePath atomically:YES];
                
                completion(newFilePath, [cloudItem.itemName stringByDeletingPathExtension], error);
            }
        }];
    }
    else{
        completion(nil, nil, nil);
    }
    
}

- (void)uploadFileWithUrl:(NSURL *)url completion:(void (^)(BOOL uploadComplete, NSError *error))completion
{
    ODItemContentRequest *contentRequest = [[[[self.odClient drive] items:@"root"] itemByPath:[[NSString stringWithFormat:@"%@", url] lastPathComponent]] contentRequest];

    [contentRequest uploadFromFile:url completion:^(ODItem *item, NSError *error){
        if (error) {
            completion(NO, error);
        } else {
            completion(YES, nil);
        }
    }];
}

@end
