//
//  DP_CloudItemLocalModel.h
//  Teleprompter
//
//  Created by Chirag Patel on 21/10/16.
//  Copyright © 2016 BlueLabelLabs. All rights reserved.
//

#import "DP_CloudItemModel.h"

@interface DP_CloudItemLocalModel : DP_CloudItemModel <DP_CloudProtocol>

@end
